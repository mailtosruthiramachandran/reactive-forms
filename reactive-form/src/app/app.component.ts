import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reactive-form';
  constructor(private fb: FormBuilder) {}
  loanForm!: FormGroup;
  ngOnInit(): void {
    this.loanForm = this.fb.group({
      'loanType' : new FormControl('', [ 
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15)
      ]),
      'loanDescription' : new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15)
      ])),
      'loans' : new FormArray([
        new FormControl('personal'),
        new FormControl('businness')
      ]),
      'address' : new FormArray([
        this.fb.group({
          'address' : new FormControl('address'),
          'city' : new FormControl('city'),
          'state' : new FormControl('state') 
        }),
        this.fb.group({
          'address' : new FormControl('address'),
          'city' : new FormControl('city'),
          'state' : new FormControl('state') 
        })
      ])
    })
    this.getValue(0);
  }


  validate() {
    console.log(this.loanForm.get('loanType')?.valid);
    console.log(this.loanForm.get('loanDescription')?.valid);
    console.log(this.loanForm.get('loanType')?.value);
    console.log(this.loanForm.valid, 'valid');
    console.log(this.loanForm.dirty, 'dirty'); // if the value is changed returns true
    console.log(this.loanForm.invalid, 'invalid'); 
    console.log(this.loanForm.pending, 'pending'); // if th validation is async and shows that it is processing the async validation
    console.log(this.loanForm.pristine, 'pristine'); // if the value is not changed 
    console.log(this.loanForm.touched, 'touched'); // eventhough value is not changed but touched return true 
    console.log(this.loanForm.untouched, 'untouched'); // true when the formscontrol is not clicked
    console.log(this.loanForm.get('loanType')?.hasError('required')); // can find whether the particular error is occured or not
    // above formstate can be used for specific field too
    console.log(this.loanForm.get('loanType')?.touched);
  }

  getControls(fromGroup : string) {
    const arr = (this.loanForm.get(fromGroup) as FormArray);  
    return this.loanForm.get(fromGroup ) as FormArray;
  }

  getValue(i : number) {
    const arr = (this.loanForm.get('loans') as FormArray);
    arr.value[i] = 'hello';
    (this.loanForm.get('loans') as FormArray).setValue(arr.value);  
  }

  addFormGroup() {
    (this.loanForm.get('address') as FormArray).push(
      this.fb.group({
        'address' : new FormControl(''),
        'state' : new FormControl(''),
        'city' : new FormControl('')
      })
    )
  }

  removeFormGroup(i : number) {
    (this.loanForm.get('address') as FormArray).removeAt(i);
  }
}
